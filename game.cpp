#include "game.h"
#include "ui_game.h"
#include "rules.h"
#include <unistd.h>
#include <QEventLoop>
#include <QTimer>

// Declare two matrixes
int **tick;
int **nTick;

// Tick number
int cTick;

// Main constructor
Game::Game(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Game)
{
    ui->setupUi(this);

    // Allocate memory
    tick  = new int *[ui -> cellTable -> rowCount()];
    nTick = new int *[ui -> cellTable -> rowCount()];
    for ( int i = 0; i < ui -> cellTable -> rowCount(); i++ )
    {
        tick[i]  = new int [ui -> cellTable -> columnCount()];
        nTick[i] = new int [ui -> cellTable -> columnCount()];
    }

    // Fill cells with black background
   for ( int i = 0; i < ui -> cellTable -> rowCount(); i++ )
   {
       for ( int j = 0; j < ui -> cellTable -> columnCount(); j++ )
       {
           ui -> cellTable -> setItem(i, j, new QTableWidgetItem);
           ui -> cellTable -> item(i,j) -> setBackground(Qt::black);
       }
   }
}

// Main destructor
Game::~Game()
{
    delete ui;
}

/*
 * On click of startButton we start the infinite loop
 * in which we call the 'checkField' function where we check
 * all elements in our first matrix and put the results in the
 * second one, so on the next iteration ( tick ) user sees on the field
 * how all cells have changed
 */
void Game::on_startButton_clicked()
{
    /*
     * Disable the start button so user can't press it multiple times
     * during the run
     */
    ui -> startButton -> setEnabled(false);

    /* EventLoop allows to avoid UI block.
     * We declare object of the class and connect stop-signal with
     * appropriate slot to allow user to stop the programm at any time.
     */
    QEventLoop myEvent;
    QObject::connect(this, SIGNAL(stopGame()), &myEvent, SLOT(quit()),
                     Qt::QueuedConnection);

    /*
     * We will use timer to stop the main loop on "Stop" keypress
     * We declare the object, connect stop-signal with appropriate slot
     * to stop the main loop by keypress.
     */
    QTimer myTimer;
    QObject::connect(this, SIGNAL(stopTimer()), &myTimer, SLOT(stop()),
                     Qt::QueuedConnection);

    myTimer.start();
    // Check if timer is active

    cTick = 0;

    while ( myTimer.isActive() )
    {
        // Call 'checkField'
        checkField(tick,nTick, ui -> cellTable -> rowCount(),
                               ui -> cellTable -> columnCount(),
                               ui -> changeBox -> value());
        // Redraw the table
        for ( int b = 0; b < ui -> cellTable -> rowCount(); b++ )
        {
            for ( int c = 0; c < ui -> cellTable -> columnCount(); c++ )
            {
                // Fill with red if the cell is 'alive'
                if ( tick[b][c] == 1 )
                    ui -> cellTable -> item(b,c) -> setBackground(Qt::red);
                // or black if it's 'dead'
                else
                    ui -> cellTable -> item(b,c) -> setBackground(Qt::black);
            }
        }

        cTick++;
        QVariant a = cTick;
        ui -> currentTick -> setText(a.toString());

        // Process all the events ( e.g. user input )
        myEvent.processEvents();

        // Sleep for a moment so user will see more smooth pattern changes
        usleep(25000);
    }
    // Reenable startbutton
    ui -> startButton -> setEnabled(true);

}

/*
 * Before the CA started user needs to define the 'first generation'.
 * To do so our programm should allow to simply click on the required cell
 * on the field to change its state.
 * So when our cellTable recieves 'onClick' signal we run the following
 * function.
 */
void Game::on_cellTable_cellClicked(int row, int column)
{
    // If the clicked cell was 'dead' we make it 'alive'
    if ( tick[row][column] == 0 )
    {
        tick[row][column] = 1;
        ui -> cellTable -> item(row, column) -> setBackground(Qt::red);
    }
    // else we do the reverse
    else
    {
        tick[row][column] = 0;
        ui -> cellTable -> item(row, column) -> setBackground(Qt::black);
    }
}

void Game::on_stopButton_clicked()
{
    // Stop timer and eventloop
    emit stopGame();
    emit stopTimer();
}
