# Game of life written in C++ and Qt. #
## Build and run instructions: ##
* Install **Qt Creator**
* Clone repository
* Open **game_of_life.pro** file in Qt Creator
* Build the project ( Ctrl+B by default )
* Launch the program ( Ctrl+R )