#include <random>
#include <rules.h>
#include "game.h"
#include <iostream>

void checkField(int **state1, int **state2, int size1, int size2, double chance)
{

    // Random numbers
    double res;
    int neighboursAlive;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution <double> unif(0.0, 1.0);

    // Return if there is no chance
    if ( chance == 0 )
        return;

    for ( int i = 0; i < size1; i++ )
    {
        for ( int j = 0; j < size2; j++ )
        {
            neighboursAlive = 0;

            for ( int b = i - 1; b <= i+1; b++)
            {
                for ( int c = j - 1; c <= j+1; c++)
                {

                    int temp1 = b;
                    int temp2 = c;

                    if ( b == -1 )
                        b = size1 - 1;
                    else if ( b == size1 )
                        b = 0;

                    if ( c == -1 )
                        c = size2 - 1;
                    else if ( c == size2 )
                        c = 0;

                    if ( state1[b][c] == 1)
                        neighboursAlive++;

                    b = temp1;
                    c = temp2;
                }
            }

            // Generate random number for every cell
            res = unif(rd);

            if ( state1[i][j] == 1 && res <= chance )
                neighboursAlive--;

            if ( neighboursAlive == 3 && res <= chance )
                state2[i][j] = 1;
            else if ( ( neighboursAlive < 2 || neighboursAlive > 3 ) &&
                      res <= chance )
                state2[i][j] = 0;
            else
                state2[i][j] = state1[i][j];
        }

    }


    // Copy curTick cells to prevTick
    for ( int i = 0; i < size1; i++ )
    {
        for ( int j = 0; j < size2; j++ )
        {
            state1[i][j] = state2[i][j];
            // And clean the state2
            state2[i][j] = 0;
        }
    }

}
