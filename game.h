#ifndef GAME_H
#define GAME_H

#include <QMainWindow>

namespace Ui {
class Game;
}

class Game : public QMainWindow
{
    Q_OBJECT

public:
    explicit Game(QWidget *parent = 0);
    ~Game();

signals:
    void stopGame();
    void stopTimer();

private slots:
    void on_startButton_clicked();
    void on_stopButton_clicked();
    void on_cellTable_cellClicked(int row, int column);

private:
    Ui::Game *ui;
};

#endif // GAME_H
